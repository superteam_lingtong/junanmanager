package com.ilingtong.junan.junanmanager.listener;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.ilingtong.junan.junanmanager.bean.BaseDataInfoBean;
import com.ilingtong.junan.junanmanager.bean.BaseDataListBean;
import com.ilingtong.junan.junanmanager.bean.BaseDataListInfoBean;

import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/18.
 * mail: wuqian@ilingtong.com
 * Description:接口返回数据后二次处理基础数据。分层剥离各类型的数据成独立列表
 */
public abstract class BaseDataCallBackListener extends AbstractActionCallbackListener<BaseDataListBean>{
    private Context context;
    private SharedPreferences sp;
    private String type;

    public BaseDataCallBackListener(Context context, String type) {
        super(context);
        this.context = context;
        this.type = type;
         sp = context.getSharedPreferences("baseDataSp", Context.MODE_PRIVATE);
    }


    public abstract void getBaseDataListBean(ArrayList<BaseDataInfoBean> data_list);
    @Override
    public void onSuccess(BaseDataListBean data) {
        ArrayList<BaseDataInfoBean> data_list = new ArrayList<>();
        for (int i = 0; i < data.base_data_list.size(); i++) {
            if (type.equals(data.base_data_list.get(i).base_data_type)){
                data_list = data.base_data_list.get(i).data_list;
                saveJson(data.base_data_list.get(i),type);
                break;
            }
        }
        getBaseDataListBean(data_list);
    }
    private void saveJson(BaseDataListInfoBean info, String type){

        String spKey = "dataListJson" + type;
        sp.edit().putString(spKey,new Gson().toJson(info)).commit();
    }
    public ArrayList<BaseDataInfoBean> getDataListFormSp(String type){
        ArrayList<BaseDataInfoBean> list = new ArrayList<>();
        String spKey = "dataListJson" + type;
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(sp.getString(spKey,""))){
            list = gson.fromJson(sp.getString(spKey,""),BaseDataListInfoBean.class).data_list;
        }
        return list;
    }
}
