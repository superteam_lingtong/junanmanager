package com.ilingtong.junan.junanmanager.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilingtong.junan.junanmanager.R;
import com.ilingtong.junan.junanmanager.bean.AuditInfoBean;
import com.ilingtong.junan.junanmanager.constant.Constants;
import com.ilingtong.junan.junanmanager.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by fengguowei on 2017/4/14.
 */

public class AuditAdapter extends BaseAdapter {
    private Context mContext;
    private List<AuditInfoBean> mList;
    private ImageLoader mImageLoader;

    public AuditAdapter(Context context, List<AuditInfoBean> list) {
        this.mContext = context;
        this.mList = list;
        mImageLoader = ImageLoader.getInstance();

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(R.layout.audit_list_item, null);
            holder.ivIcon = (ImageView) view.findViewById(R.id.iv_icon);
            holder.txtName = (TextView) view.findViewById(R.id.txt_name);
            holder.txtDate = (TextView) view.findViewById(R.id.txt_date);
            holder.txtVeteransNum = (TextView) view.findViewById(R.id.txt_veterans_num);
            holder.txtIDCard = (TextView) view.findViewById(R.id.txt_id_num);
            holder.txtArmType = (TextView) view.findViewById(R.id.txt_arms_type);
            holder.txtVeteransType = (TextView) view.findViewById(R.id.txt_veterans_type);
            holder.lvArmsType = (RelativeLayout) view.findViewById(R.id.arms_type_layout);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        AuditInfoBean auditInfoBean = mList.get(position);
        mImageLoader.displayImage(auditInfoBean.getApplicant_head_photo_url(), holder.ivIcon, ImageOptionsUtils.getThumbnailOptions());
        holder.txtName.setText(auditInfoBean.getName());
        holder.txtDate.setText(auditInfoBean.getApply_date());
        holder.txtVeteransNum.setText(String.format(mContext.getResources().getString(R.string.veterans_num), auditInfoBean.getVeterans_card_number()));
        holder.txtIDCard.setText(String.format(mContext.getString(R.string.id_num), auditInfoBean.getId_card()));
        if (!Constants.VETERANAS_YES.equals(auditInfoBean.getIs_veterans())) {
            holder.lvArmsType.setVisibility(View.GONE);
        } else {
            holder.lvArmsType.setVisibility(View.VISIBLE);
            holder.txtArmType.setText(mContext.getResources().getString(R.string.veterans));
            if (!(TextUtils.isEmpty(auditInfoBean.getArm()) && TextUtils.isEmpty(auditInfoBean.getApplicant_type()))) {
                holder.txtVeteransType.setText(auditInfoBean.getArm() + "-" + auditInfoBean.getApplicant_type());
            } else {
                holder.txtVeteransType.setText(auditInfoBean.getArm() + auditInfoBean.getApplicant_type());
            }
        }
        return view;
    }

    public class ViewHolder {
        private ImageView ivIcon;
        private TextView txtName;
        private TextView txtDate;
        private TextView txtVeteransNum;
        private TextView txtIDCard;
        private TextView txtArmType;
        private TextView txtVeteransType;
        private RelativeLayout lvArmsType;
    }
}
