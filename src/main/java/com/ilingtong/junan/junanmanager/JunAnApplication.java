package com.ilingtong.junan.junanmanager;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;

import com.ilingtong.junan.junanmanager.bean.ConfigInfoBean;
import com.ilingtong.junan.junanmanager.constant.Constants;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.Stack;


public class JunAnApplication extends Application {
    private static Context context = null;
    public static JunAnApplication instance;//Application的单例
    private static Stack<Activity> activityStack;//应用程序打开的过的Activity
    public ConfigInfoBean mConfigInfoBean;
    private SharedPreferences mConfigSp;


    public static JunAnApplication getApplication() {
        if (instance == null) {
            instance = new JunAnApplication();
        }
        return instance;
    }

    public static void setContext(Context context) {
        JunAnApplication.context = context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //android7.0无法安装
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContext(getApplicationContext());
        mConfigSp = getSharedPreferences("configInfo", Context.MODE_PRIVATE);
        mConfigInfoBean = new ConfigInfoBean(mConfigSp);
        mConfigInfoBean.initConfigInfo(Constants.APP_INNER_NO, "");
        initImageLoader(getApplicationContext());
        instance = this;
    }


    public ConfigInfoBean getmConfigInfoBean() {
        if (mConfigInfoBean == null) {
            mConfigInfoBean = new ConfigInfoBean(mConfigSp);
        }
        return mConfigInfoBean;
    }

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<Activity>();
        }
        activityStack.add(activity);
    }

    public void removeActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity = null;
        }
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = activityStack.size(); i < size; i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    /**
     * 退出应用程序
     */
    public void AppExit(Context context) {
        try {
            finishAllActivity();
            ActivityManager activityMgr = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            activityMgr.restartPackage(context.getPackageName());
            System.exit(0);
        } catch (Exception e) {
        }
    }

    /**
     * 初始化ImageLoader
     *
     * @param context
     */
    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        ImageLoader.getInstance().init(config);
    }

}
