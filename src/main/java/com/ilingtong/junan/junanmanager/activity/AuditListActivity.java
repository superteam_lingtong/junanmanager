package com.ilingtong.junan.junanmanager.activity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilingtong.junan.junanmanager.AppActionManager;
import com.ilingtong.junan.junanmanager.JunAnApplication;
import com.ilingtong.junan.junanmanager.R;
import com.ilingtong.junan.junanmanager.adapter.AuditAdapter;
import com.ilingtong.junan.junanmanager.bean.AuditInfoBean;
import com.ilingtong.junan.junanmanager.bean.AuditListResultBean;
import com.ilingtong.junan.junanmanager.constant.Constants;
import com.ilingtong.junan.junanmanager.listener.AbstractActionCallbackListener;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.loadmore.LoadMoreContainer;
import in.srain.cube.views.loadmore.LoadMoreHandler;
import in.srain.cube.views.loadmore.LoadMoreListViewContainer;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * @author fengguowei
 * @ClassName: AuditListActivity
 * @Description: 审核列表
 * @date 2017/4/14 15:43
 */

public class AuditListActivity extends BaseActivity implements View.OnClickListener {
    private ImageView ivBack;
    private TextView txtLeftTitle;
    private ImageView ivMore;
    private ListView mAuditListView;
    private PtrClassicFrameLayout mPtrFrame;//刷新控件
    private LoadMoreListViewContainer loadMoreListViewContainer;//加载更多控件
    private AbstractActionCallbackListener<AuditListResultBean> mAbstractActionCallbackListener;
    private String mSortKey;
    private List<AuditInfoBean> mAuditInfoList;
    private AuditAdapter mAuditAdapter;
    private PopupWindow mPopupWindow;
    private View mPopView;
    private Display mDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_list_layout);
        init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0x01 && resultCode == 0x02) {
            if (data != null) {
                for (AuditInfoBean auditInfoBean : mAuditInfoList) {
                    if (data.getExtras().getString("applicant_id").equals(auditInfoBean.getApplicant_id()) && data.getExtras().getString("apply_date").equals(auditInfoBean.getApply_date())) {
                        mAuditInfoList.remove(auditInfoBean);
                        break;
                    }
                }
                if (mAuditInfoList.size() > 0) {
                    mSortKey = mAuditInfoList.get(mAuditInfoList.size() - 1).getSort_key();
                } else {
                    mSortKey = null;
                    loadMoreListViewContainer.loadMoreFinish(true, false);
                }
                mAuditAdapter.notifyDataSetChanged();
            }
        }
    }

    //初始化控件
    private void init() {
        mAuditInfoList = new ArrayList<>();
        ivBack = (ImageView) findViewById(R.id.iv_back);
        txtLeftTitle = (TextView) findViewById(R.id.txt_left_title);
        ivMore = (ImageView) findViewById(R.id.iv_more);
        txtLeftTitle.setText(getText(R.string.audit_list_title));
        mAuditListView = (ListView) findViewById(R.id.lv_audit_list);
        mPtrFrame = (PtrClassicFrameLayout) findViewById(R.id.audit_frame_refresh);
        ivBack.setOnClickListener(this);
        ivMore.setOnClickListener(this);
        mAbstractActionCallbackListener = new AbstractActionCallbackListener<AuditListResultBean>(this) {
            @Override
            public void onSuccess(AuditListResultBean data) {
                if (mSortKey == null) {
                    mAuditInfoList.clear();
                }
                mAuditInfoList.addAll(data.getAuditing_list());
                mAuditAdapter.notifyDataSetChanged();
                if (mSortKey == null) {
                    mPtrFrame.refreshComplete();
                }
                if (mAuditInfoList.size() > 0) {
                    mSortKey = mAuditInfoList.get(mAuditInfoList.size() - 1).getSort_key();
                } else {
                    loadMoreListViewContainer.loadMoreFinish(true, false);
                    return;
                }
                if (Integer.parseInt(data.getData_total_count()) == mAuditInfoList.size()) {
                    loadMoreListViewContainer.loadMoreFinish(false, false);
                } else {
                    loadMoreListViewContainer.loadMoreFinish(false, true);
                }
            }

            @Override
            public void onFailure(String errorEvent, String message) {
                super.onFailure(errorEvent, message);
                if (mSortKey == null) {
                    mPtrFrame.refreshComplete();
                }
                loadMoreListViewContainer.loadMoreFinish(false, true);
            }
        };
        //设置订单刷新
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                //开始刷新
                mPtrFrame.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSortKey = null;
                        getAuditList();
                    }
                }, 500);
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, mAuditListView, header);//第二个参数为ListView 为content时会有问题
            }
        });
        //加载更多
        loadMoreListViewContainer = (LoadMoreListViewContainer) findViewById(R.id.load_more_list_view_container);
        loadMoreListViewContainer.useDefaultFooter();
        loadMoreListViewContainer.setLoadMoreHandler(new LoadMoreHandler() {
            @Override
            public void onLoadMore(LoadMoreContainer loadMoreContainer) {
                if (mAuditInfoList.size() > 0) {
                    getAuditList();
                }
            }
        });
        mAuditListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AuditDetailActivity.launchActivity(AuditListActivity.this, mAuditInfoList.get(position).getApply_date(), mAuditInfoList.get(position).getApplicant_id());
            }
        });
        mAuditAdapter = new AuditAdapter(this, mAuditInfoList);
        mAuditListView.setAdapter(mAuditAdapter);
        getAuditList();
    }

    /**
     * @param
     * @return
     * @throws
     * @author fengguowei
     * @date 2017/4/14 18:21
     * @Title: ${enclosing_method}
     * @Description: 获取审核数据列表
     */

    private void getAuditList() {
        AppActionManager.getAuditDataList(this, mSortKey, Constants.FORWORD_DONW, Constants.FETCH_COUNT, true, mAbstractActionCallbackListener, this);
    }

    //显示退出PopupWindow
    private void showExitPop() {
        mDisplay = getWindowManager().getDefaultDisplay();
        if (mPopView == null) {
            mPopView = LayoutInflater.from(AuditListActivity.this).inflate(R.layout.exit_app_layout, null);
            mPopView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JunAnApplication.getApplication().AppExit(AuditListActivity.this);
                    mPopupWindow.dismiss();
                }
            });
        }
        if (mPopupWindow == null) {
            mPopupWindow = new PopupWindow(mPopView, mDisplay.getWidth() / 3, RelativeLayout.LayoutParams.WRAP_CONTENT);
            mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
            mPopupWindow.setOutsideTouchable(true);
        }
        mPopupWindow.showAsDropDown(ivMore, 0, ((int) (0.5F + 8 * getResources().getDisplayMetrics().density)));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_more:
                showExitPop();
                break;
            default:
                break;
        }
    }
}
