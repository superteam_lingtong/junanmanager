package com.ilingtong.junan.junanmanager.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.junanmanager.AppActionManager;
import com.ilingtong.junan.junanmanager.R;
import com.ilingtong.junan.junanmanager.bean.AuditDetailInfoBean;
import com.ilingtong.junan.junanmanager.bean.AuditDetailResultBean;
import com.ilingtong.junan.junanmanager.constant.Constants;
import com.ilingtong.junan.junanmanager.listener.AbstractActionCallbackListener;
import com.ilingtong.junan.junanmanager.utils.ImageOptionsUtils;
import com.ilingtong.junan.junanmanager.utils.ToastUtils;
import com.ilingtong.junan.junanmanager.widget.GestureImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * @author fengguowei
 * @ClassName: AuditDetailActivity
 * @Description: 审核详情
 * @date 2017/4/14 15:42
 */

public class AuditDetailActivity extends BaseActivity implements View.OnClickListener {
    private ImageView ivBack;
    private TextView txtTitleLeft;
    private ImageView ivMore;
    private TextView txtName;
    private TextView txtIDNum;
    private TextView txtVeteransNum;
    private TextView txtArmType;
    private TextView txtVeteransType;
    private TextView txtPhoneNum;
    private ImageView ivVeteransCard;
    private ImageView ivIDCardPositive;
    private ImageView ivIDCardOpposite;
    private Button btnAgree;
    private Button btnRefuse;
    private AbstractActionCallbackListener<AuditDetailResultBean> mAbstractActionCallbackListener;
    private String mApplyDate;//申请日期
    private String mApplicantId;//申请人Id
    //拒绝/同意回调
    private AbstractActionCallbackListener mAuditActionCallbackListener;
    private AuditDetailInfoBean mAuditDetailInfoBean;
    private ImageLoader mImageLoader;
    private Dialog mGraphicDialog;
    private View mGraphicView;
    private GestureImageView ivGraphic;
    private boolean isOperationSuccess = false;//审核操作是否成功

    /**
     * @param apply_date   申请日期
     * @param applicant_id 申请人Id
     * @return
     * @throws
     * @author fengguowei
     * @date 2017/4/14 18:52
     * @Title: ${enclosing_method}
     * @Description: 跳转
     */

    public static void launchActivity(Activity activity, String apply_date, String applicant_id) {
        Intent intent = new Intent(activity, AuditDetailActivity.class);
        intent.putExtra("apply_date", apply_date);
        intent.putExtra("applicant_id", applicant_id);
        activity.startActivityForResult(intent, 0x01);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_detail);
        init();
    }

    private void init() {
        mImageLoader = ImageLoader.getInstance();
        mApplyDate = getIntent().getStringExtra("apply_date");
        mApplicantId = getIntent().getStringExtra("applicant_id");
        ivBack = (ImageView) findViewById(R.id.iv_back);
        txtTitleLeft = (TextView) findViewById(R.id.txt_left_title);
        ivMore = (ImageView) findViewById(R.id.iv_more);
        txtName = (TextView) findViewById(R.id.txt_name);
        txtIDNum = (TextView) findViewById(R.id.txt_id_num);
        txtVeteransNum = (TextView) findViewById(R.id.txt_veterans_num);
        txtArmType = (TextView) findViewById(R.id.txt_arms_type);
        txtVeteransType = (TextView) findViewById(R.id.txt_veterans_type);
        txtPhoneNum = (TextView) findViewById(R.id.txt_phone_num);
        ivVeteransCard = (ImageView) findViewById(R.id.iv_veterans_card);
        ivIDCardPositive = (ImageView) findViewById(R.id.iv_id_card_positive);
        ivIDCardOpposite = (ImageView) findViewById(R.id.iv_id_card_opposite);
        btnAgree = (Button) findViewById(R.id.btn_audit_agree);
        btnRefuse = (Button) findViewById(R.id.btn_audit_refuse);
        ivBack.setOnClickListener(this);
        txtTitleLeft.setText(getText(R.string.audit_detail_title));
        ivMore.setVisibility(View.GONE);
        ivVeteransCard.setOnClickListener(this);
        ivIDCardPositive.setOnClickListener(this);
        ivIDCardOpposite.setOnClickListener(this);
        btnAgree.setOnClickListener(this);
        btnRefuse.setOnClickListener(this);
        mAbstractActionCallbackListener = new AbstractActionCallbackListener<AuditDetailResultBean>(this) {
            @Override
            public void onSuccess(AuditDetailResultBean data) {
                mAuditDetailInfoBean = data.getAuditing_info();
                txtName.setText(data.getAuditing_info().getName());
                txtIDNum.setText(data.getAuditing_info().getId_card());
                txtVeteransNum.setText(data.getAuditing_info().getVeterans_card_number());
                txtArmType.setText(data.getAuditing_info().getArm());
                txtVeteransType.setText(data.getAuditing_info().getApplicant_type());
                txtPhoneNum.setText(data.getAuditing_info().getPhone());
                mImageLoader.displayImage(mAuditDetailInfoBean.getDischarged_pic(), ivVeteransCard, ImageOptionsUtils.getCertificatePhotoOptions());
                mImageLoader.displayImage(mAuditDetailInfoBean.getId_card_positive_pic(), ivIDCardPositive, ImageOptionsUtils.getIDPhotoOptions());
                mImageLoader.displayImage(mAuditDetailInfoBean.getId_card_opposite_pic(), ivIDCardOpposite, ImageOptionsUtils.getIDPhotoOptions());
                btnAgree.setEnabled(true);
                btnRefuse.setEnabled(true);
            }

            @Override
            public void onFailure(String errorEvent, String message) {
                super.onFailure(errorEvent, message);
            }
        };
        mAuditActionCallbackListener = new AbstractActionCallbackListener(this) {
            @Override
            public void onSuccess(Object data) {
                isOperationSuccess = true;
                btnAgree.setEnabled(false);
                btnRefuse.setEnabled(false);
                ToastUtils.showLongToast(AuditDetailActivity.this, getString(R.string.audit_success_tips));
            }

            @Override
            public void onFailure(String errorEvent, String message) {
                super.onFailure(errorEvent, message);
                ToastUtils.showLongToast(AuditDetailActivity.this, getString(R.string.audit_fail_tips));
            }
        };
        getAuditDetail();
    }

    /**
     * @param
     * @return
     * @throws
     * @author fengguowei
     * @date 2017/4/14 18:54
     * @Title: ${enclosing_method}
     * @Description: 获取审核详情
     */

    private void getAuditDetail() {
        AppActionManager.getAuditDetail(this, mApplyDate, mApplicantId, true, mAbstractActionCallbackListener, this);
    }

    /**
     * @param status 同意/拒绝
     * @return
     * @throws
     * @author fengguowei
     * @date 2017/4/14 18:55
     * @Title: ${enclosing_method}
     * @Description: 审核操作
     */
    private void doAuditAction(String status, String reason) {
        AppActionManager.auditAction(AuditDetailActivity.this, mApplyDate, mApplicantId, status, reason, true, mAuditActionCallbackListener, this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isOperationSuccess) {
                Intent intent=new Intent();
                intent.putExtra("applicant_id",mApplicantId);
                intent.putExtra("apply_date",mApplyDate);
                setResult(0x02,intent);
            }
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                if (isOperationSuccess) {
                    Intent intent=new Intent();
                    intent.putExtra("applicant_id",mApplicantId);
                    intent.putExtra("apply_date",mApplyDate);
                    setResult(0x02,intent);
                }
                finish();
                break;
            case R.id.iv_veterans_card:
                if (mAuditDetailInfoBean != null) {
                    showDialog(mAuditDetailInfoBean.getDischarged_pic(), ImageOptionsUtils.getCertificatePhotoOptions());
                }
                break;
            case R.id.iv_id_card_positive:
                if (mAuditDetailInfoBean != null) {
                    showDialog(mAuditDetailInfoBean.getId_card_positive_pic(), ImageOptionsUtils.getIDPhotoOptions());
                }
                break;
            case R.id.iv_id_card_opposite:
                if (mAuditDetailInfoBean != null) {
                    showDialog(mAuditDetailInfoBean.getId_card_opposite_pic(), ImageOptionsUtils.getIDPhotoOptions());
                }
                break;
            case R.id.high_quality_pic:
                mGraphicDialog.dismiss();
                break;
            case R.id.btn_audit_agree:
                doAuditAction(Constants.AUDIT_AGREE, "");
                break;
            case R.id.btn_audit_refuse:
                doAuditAction(Constants.AUDIT_REFUSE, "");
                break;
            default:
                break;
        }
    }

    /**
     * @param url 图片地址
     * @author fengguowei
     * @date 2017/4/20 11:05
     * @Title: ${enclosing_method}
     * @Description: 大图浏览
     */

    private void showDialog(String url, DisplayImageOptions displayImageOptions) {
        if (mGraphicDialog == null) {
            mGraphicDialog = new Dialog(AuditDetailActivity.this, android.R.style.Theme_NoTitleBar);
            mGraphicDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    ivGraphic.reset();
                }
            });
        }
        if (mGraphicView == null) {
            mGraphicView = LayoutInflater.from(AuditDetailActivity.this).inflate(R.layout.graphic_view_layout, null);
            ivGraphic = (GestureImageView) mGraphicView.findViewById(R.id.high_quality_pic);
            ivGraphic.setOnClickListener(this);
            mGraphicDialog.setContentView(mGraphicView);
        }
        mImageLoader.displayImage(url, ivGraphic, displayImageOptions, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                ivGraphic.setScalable(false);//设置图片不可进行缩放
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, final View view, Bitmap loadedImage) {
                if (loadedImage != null) {
                    ivGraphic.setScalable(true);//设置图片可进行缩放
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
        mGraphicDialog.show();
    }
}