package com.ilingtong.junan.junanmanager.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ilingtong.junan.junanmanager.AppActionManager;
import com.ilingtong.junan.junanmanager.JunAnApplication;
import com.ilingtong.junan.junanmanager.R;
import com.ilingtong.junan.junanmanager.bean.UserInfoBean;
import com.ilingtong.junan.junanmanager.bean.VersionInfoBean;
import com.ilingtong.junan.junanmanager.listener.AbstractActionCallbackListener;
import com.ilingtong.junan.junanmanager.service.VersionUpdateService;
import com.ilingtong.junan.junanmanager.utils.ToastUtils;
import com.ilingtong.junan.junanmanager.utils.VersionUpdateUtils;

import java.io.File;

/**
 * Created by wuqian on 2017/4/14.
 * mail: wuqian@ilingtong.com
 * Description:登录
 */
public class LoginActivity extends BaseActivity implements VersionUpdateUtils.ICheckVersion {
    private EditText editAccount;
    private EditText editPassword;
    private Button btnLogin;
    private AbstractActionCallbackListener<UserInfoBean> mAbstractCallbackListener;
    private VersionUpdateUtils mVersionUpdateUtils;
    private VersionInfoBean mVersionInfoBean;//版本信息
    public static int loading_process = 0;
    private ProgressBar pb;//下载进度条
    private TextView txtTips;
    private Thread loadFileThread;
    private Dialog downloadDialog;
    private Dialog downloadDialogTips;
    private boolean isUpdateCancel = false;//handler发送消息可能会有延迟，防止手动取消时继续进行安装
    private Dialog mPermissionTipsDialog;
    private View mTipsView;
    private Button btnOK;
    private SharedPreferences mSP;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case VersionUpdateUtils.LOADING:
                    //进度条
                    pb.setProgress(msg.arg1);
                    loading_process = msg.arg1;
                    txtTips.setText(String.format(getString(R.string.common_update_loading_txt), loading_process));
                    break;
                case VersionUpdateUtils.COMPLETED:
                    if (null != downloadDialog) {
                        downloadDialog.dismiss();
                        downloadDialog = null;
                    }
                    //安装APK
                    if (!isUpdateCancel) {
                        installApk();
                    }
                    break;
                case VersionUpdateUtils.CANCELED:
                    //取消
                    ToastUtils.showLongToast(LoginActivity.this, getString(R.string.update_canceled));
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle saveBundleInstance) {
        super.onCreate(saveBundleInstance);
        setContentView(R.layout.activity_login_layout);
        init();
        checkPermission();
    }

    //初始化控件
    private void init() {
        mSP = getSharedPreferences("configInfo", Context.MODE_PRIVATE);
        mVersionUpdateUtils = new VersionUpdateUtils(this, this, this);
        editAccount = (EditText) findViewById(R.id.edit_login_acount);
        editPassword = (EditText) findViewById(R.id.edit_login_psd);
        btnLogin = (Button) findViewById(R.id.btn_login);
        if (!TextUtils.isEmpty(mSP.getString("account", ""))) {
            editAccount.setText(mSP.getString("account", ""));
            editPassword.requestFocus();
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        mAbstractCallbackListener = new AbstractActionCallbackListener<UserInfoBean>(this) {
            @Override
            public void onSuccess(UserInfoBean data) {
                mSP.edit().putString("account", editAccount.getText().toString().trim()).commit();
                JunAnApplication.instance.getmConfigInfoBean().setToken(data.getToken());
                JunAnApplication.instance.getmConfigInfoBean().setUserid(data.getUser_id());
                Intent intent = new Intent(LoginActivity.this, AuditListActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(String errorEvent, String message) {
                super.onFailure(errorEvent, message);
                ToastUtils.showLongToast(LoginActivity.this, message);
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mVersionUpdateUtils.checkVersion();
            } else {
                //需延时弹出 立即弹出自定义dialog会受系统权限对话dialog影响出现背景为白色
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        checkPermission();
                    }
                }, 50);
            }
        }
    }

    @Override
    public void getVersionSuccess(VersionInfoBean versionInfoBean) {
        //判断是否需要更新
        if (!versionInfoBean.recently_version_no.equals(mVersionUpdateUtils.getVersionName())) {
            mVersionInfoBean = versionInfoBean;
            updateVersion();
        }
    }

    @Override
    public void downLoadFileChanged(int flag, int progress) {
        Message message = handler.obtainMessage();
        message.what = flag;
        message.arg1 = progress;
        handler.sendMessage(message);
    }

    /**
     * @param
     * @return
     * @throws
     * @author fengguowei
     * @date 2017/4/14 17:31
     * @Title: ${enclosing_method}
     * @Description: 登录
     */

    public void login() {
        AppActionManager.login(this, editAccount.getText().toString().trim(), editPassword.getText().toString().trim(), true, mAbstractCallbackListener, this);
    }

    //申请写权限
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(LoginActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if (mPermissionTipsDialog == null) {
                    mPermissionTipsDialog = new Dialog(LoginActivity.this);
                }
                if (mTipsView == null) {
                    mTipsView = LayoutInflater.from(LoginActivity.this).inflate(R.layout.permission_tips_dialog_layout, null);
                    mPermissionTipsDialog.setContentView(mTipsView);
                    Window window = mPermissionTipsDialog.getWindow();
                    WindowManager.LayoutParams params = window.getAttributes();
                    params.gravity = Gravity.CENTER;
                    params.width = (int) (0.5F + 280 * getResources().getDisplayMetrics().density);
                    window.setAttributes(params);
                    btnOK = (Button) mTipsView.findViewById(R.id.btn_sure);
                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mPermissionTipsDialog.dismiss();
                            ActivityCompat.requestPermissions(LoginActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    1);

                        }
                    });
                    mPermissionTipsDialog.setCancelable(false);
                }
                mPermissionTipsDialog.show();
            } else {
                mVersionUpdateUtils.checkVersion();
            }
        } else {
            mVersionUpdateUtils.checkVersion();
        }

    }
    //安装APK

    private void installApk() {
        finish();
        File file = new File(Environment.getExternalStorageDirectory(), VersionUpdateUtils.FILE_NAME);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }

    //版本更新
    private void updateVersion() {
        if (null == mVersionInfoBean) {
            return;
        }
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(
                R.layout.version_update_layout, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Button btCancel = (Button) ll.findViewById(R.id.version_btn_cancle);
        Button btSure = (Button) ll.findViewById(R.id.version_btn_sure);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadDialogTips.dismiss();
                if (isForceUpdate()) {
                    //有强制更新禁止使用应用
                    finish();
                }
            }
        });
        btSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadDialogTips.dismiss();
                showProgressDialog();
            }
        });
        downloadDialogTips = builder.show();
        downloadDialogTips.setCancelable(false);
        downloadDialogTips.setContentView(ll);
    }

    /**
     * 是否强制更新
     *
     * @return true 强制更新；false 不强制更新
     */
    public boolean isForceUpdate() {
        if (null == mVersionInfoBean || "".equals(mVersionInfoBean.recently_version_no)) {
            return false;
        }
        String[] newFlagsTemp = mVersionInfoBean.recently_version_no.split("\\.");
        String[] localFlagsTemp = mVersionUpdateUtils.getVersionName().split("\\.");
        int[] newFlags = new int[newFlagsTemp.length];
        int[] localFlags = new int[newFlagsTemp.length];
        for (int i = 0; i < newFlagsTemp.length; i++) {
            newFlags[i] = Integer.parseInt(newFlagsTemp[i]);
            localFlags[i] = Integer.parseInt(localFlagsTemp[i]);
        }
        if (newFlags[0] > localFlags[0] || newFlags[1] > localFlags[1]) {
            return true;
        } else if (newFlags[2] > localFlags[2]) {
            return false;
        }
        return false;
    }

    //初始化文件下载进度UI
    public void showProgressDialog() {
        if ("".equals(mVersionInfoBean.recently_version_link)) {
            //下载地址为空
            ToastUtils.showLongToast(LoginActivity.this, getString(R.string.update_url_invalid));
            return;
        }
        loading_process = 0;
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(
                R.layout.download_apk_progress_layout, null);
        pb = (ProgressBar) ll.findViewById(R.id.down_pb);
        txtTips = (TextView) ll.findViewById(R.id.tv);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(ll);
        builder.setTitle(getString(R.string.common_update_alert_title));
        builder.setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadFileThread.interrupt();
                        dialog.dismiss();
                        isUpdateCancel = true;
                        File file = new File(Environment.getExternalStorageDirectory(), VersionUpdateUtils.FILE_NAME);
                        if (file.exists()) {
                            file.delete();
                        }
                        if (isForceUpdate()) {
                            finish();
                        }
                    }
                });
        builder.setPositiveButton(getString(R.string.common_update_alert_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(LoginActivity.this, VersionUpdateService.class);
                startService(intent);
                dialogInterface.dismiss();
            }
        });


        downloadDialog = builder.show();
        downloadDialog.setCancelable(false);
        //开启线程根据url请求apk
        loadFileThread = new Thread() {
            public void run() {
                mVersionUpdateUtils.loadFile(mVersionInfoBean);
            }
        };
        loadFileThread.start();
    }
}
