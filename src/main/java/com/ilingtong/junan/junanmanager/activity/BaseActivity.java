package com.ilingtong.junan.junanmanager.activity;

import android.app.Activity;
import android.os.Bundle;

import com.ilingtong.junan.junanmanager.JunAnApplication;
import com.ilingtong.junan.junanmanager.listener.RequestManager;
import com.ilingtong.junan.junanmanager.listener.RequestTagListener;

import java.util.ArrayList;
/**
* @ClassName: BaseActivity
* @Description: 基类Activity
* @author fengguowei
* @date 2017/4/14 15:43
*/

public abstract class BaseActivity extends Activity implements RequestTagListener {
    /**
     * 当前页面请求Tag集合
     */
    private ArrayList<String> mRequestTagList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JunAnApplication.getApplication().addActivity(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        for (int i = 0; i < mRequestTagList.size(); i++) {
            RequestManager.getInstance(this.getApplicationContext()).
                    getRequestQueue().cancelAll(mRequestTagList.get(i));
        }
        JunAnApplication.getApplication().removeActivity(this);
    }
    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void addRequestTag(String tag) {
        mRequestTagList.add(tag);
    }
}