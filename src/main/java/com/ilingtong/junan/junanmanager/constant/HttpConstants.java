package com.ilingtong.junan.junanmanager.constant;

import com.ilingtong.junan.junanmanager.JunAnApplication;

/**
 * Created by wuqian on 2016/8/8.
 * mail: wuqian@ilingtong.com
 * Description:用来定义接口相关的常量
 */
public class HttpConstants {
    public static String HTTP_ADDRESS = "http://www.tongler.cn/api/2.0/";
    //测试版
    //public static String HTTP_ADDRESS = "http://www.tongler.cn/api/2.0/";
    public static String SYSTEM_URL=HTTP_ADDRESS+"system/";
    public static String JUNAN_LOGIN_URL = SYSTEM_URL + "jam_login";//登录
    public static String UPDATEVERSION_URL = SYSTEM_URL + "version_check";//检测版本
    public static String TOKEN_URL = HTTP_ADDRESS + JunAnApplication.instance.mConfigInfoBean.getToken() + "/";
    public static String GET_AUDIT_LIST_URL = TOKEN_URL + "jam/auditing_list";//审核列表
    public static String GET_AUDIT_DETAIL_URL = TOKEN_URL + "jam/auditing_info";//审核详情
    public static String AUDIT_ACTION_URL = TOKEN_URL + "jam/auditing";//审核操作
}
