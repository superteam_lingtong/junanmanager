package com.ilingtong.junan.junanmanager.constant;

/**
 * Created by wuqian on 2016/8/9.
 * mail: wuqian@ilingtong.com
 * Description:错误类型
 */
public class ErrorEvent {
    /**
     * 网络/接口调用/json结果处理 的错误类型 errorEvent
     **/
    public static final String NETWORK_TOKEN_FAIL_EVENT = "1002";  //token失效
    public static final String NETWORK_OTHER_ERROR_EVENT = "1001";    //接口返回结果失败
    public static final String NETWORK_VOLLEY_ERROR_EVENT = "1003";   //volley抛出的异常。UnsupportedEncodingException/JsonSyntaxException/timeout等
    public static final String PARAM_NULL = "1004";   //入口参数为空
    public static final String PARAM_ILLEGAL = "1005";   //参数不合法
}
