package com.ilingtong.junan.junanmanager.constant;

/**
 * Created by wuqian on 2016/8/3.
 * mail: wuqian@ilingtong.com
 * Description:常量定义class
 */
public class Constants {
    public static String APP_INNER_NO = "08";
    /**
     * Http connect time out time
     */
    public static final int CONNECT_TIMEOUT_MS = 40000;

    public static final String SUCCESS = "1";
    public static final String FETCH_COUNT = "10";     //列表接口每次请求数据条数
    //翻页
    public static final String FORWORD_DONW = "1";  //向下翻页，加载更多
    public static final String AUDIT_AGREE = "1";  //审核同意
    public static final String AUDIT_REFUSE = "2";  //审核拒绝

    //是否。也表示接口调用成功或失败
    public static final String YES = "0";  // 是/成功
    public static final String NO = "1";
    public static final String TOKEN_FAIL = "2";  //接口返回错误类型 。return_flag为该值时表示token失效
    public static final String PHONE = "3";  //移动平台编号 -- 手机。用户版本更新接口，区分phone和pad

    public static final String VETERANAS_YES= "0";  //是退伍军人


}
