package com.ilingtong.junan.junanmanager.http;

import android.app.Dialog;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.ilingtong.junan.junanmanager.listener.AbstractActionCallbackListener;
import com.ilingtong.junan.junanmanager.listener.RequestManager;
import com.ilingtong.junan.junanmanager.listener.RequestTagListener;
import com.ilingtong.junan.junanmanager.utils.DialogUtils;

import java.util.Map;

/**
 * Created by wuqian on 2016/8/9.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class RequestHelper {
    private static RequestHelper mInstance;

    public static RequestHelper getmInstance() {
        if (mInstance == null) {
            mInstance = new RequestHelper();
        }
        return mInstance;
    }

    /**
     * 构造函数 处理下拉刷新的请求，先判断本次请求与上一次请求相差的时间，如果小于给定时间，就用原来的数据，否则就从服务器拿去
     *
     * @param context           上下文对象
     * @param url               请求地址
     * @param method            请求方法
     * @param paramMap          参数map
     * @param headMap           头部参数
     * @param objectClass       接口返回结果实例
     * @param callbackListener  响应回调
     * @param tagListener       tagListener 请求tag监听事件，（给所有的请求添加tag，如果发起请求的activity或者fragment销毁了，就结束当前的请求）
     * @param allowGetFormCache 是否允许从本地获取数据
     * @param <T>
     */
    public <T> void handleFlushRequest(Context context, String url, int method, final Map<String, String> paramMap, final Map<String, String> headMap, Class<T> objectClass,
                                       AbstractActionCallbackListener<T> callbackListener, RequestTagListener tagListener, boolean allowGetFormCache, boolean isShowDialog) {
        Dialog dialog = DialogUtils.createLoadingDialog(context);

        BaseGsonRequest<T> request = new BaseGsonRequest<T>(method, url, objectClass, callbackListener, allowGetFormCache, dialog) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> paramsMap = super.getParams();
                paramsMap.putAll(paramMap);
                return paramsMap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = super.getHeaders();
                if (headMap != null) {
                    headerMap.putAll(headMap);
                }
                return headerMap;
            }
        };
        request.setTag(url);
        if (isShowDialog) {
            dialog.show();
        }
        RequestManager.getInstance(context).addToRequestQueue(request);
        if (null != tagListener) {
            tagListener.addRequestTag(url);
        }
    }

    /**
     * 构造函数
     * 适用于当前接口，目前接口均不需要传入head参数。
     *
     * @param context           上下文对象
     * @param url               请求地址
     * @param paramMap          参数map
     * @param objectClass       接口返回结果实例
     * @param callbackListener  响应回调
     * @param tagListener       tagListener 请求tag监听事件，（给所有的请求添加tag，如果发起请求的activity或者fragment销毁了，就结束当前的请求）
     * @param allowGetFormCache 是否允许从本地获取数据
     * @param method            post or get
     * @param isShowDialog      是否显示进度条
     * @param <T>
     */
    public <T> void handleFlushRequest(Context context, String url, int method, final Map<String, String> paramMap, Class<T> objectClass,
                                       AbstractActionCallbackListener<T> callbackListener, RequestTagListener tagListener, boolean allowGetFormCache, boolean isShowDialog) {
        handleFlushRequest(context, url, method, paramMap, null, objectClass, callbackListener, tagListener, allowGetFormCache, isShowDialog);
    }

    /**
     * 构造函数 不需要缓存
     * 适用于当前接口，目前接口均不需要传入head参数。
     *
     * @param context          上下文对象
     * @param url              请求地址
     * @param paramMap         参数map
     * @param objectClass      接口返回结果实例
     * @param callbackListener 响应回调
     * @param tagListener      tagListener 请求tag监听事件，（给所有的请求添加tag，如果发起请求的activity或者fragment销毁了，就结束当前的请求）
     * @param method           post or get
     * @param isShowDialog     是否显示进度条
     * @param <T>
     */
    public <T> void handleFlushRequest(Context context, String url, int method, final Map<String, String> paramMap, Class<T> objectClass,
                                       AbstractActionCallbackListener<T> callbackListener, RequestTagListener tagListener, boolean isShowDialog) {
        handleFlushRequest(context, url, method, paramMap, objectClass, callbackListener, tagListener, false, isShowDialog);
    }

    /**
     * send post请求。
     *
     * @param context          上下文对象
     * @param url              请求地址
     * @param paramMap         参数map
     * @param objectClass      接口返回结果实例
     * @param callbackListener 接口返回回调
     * @param tagListener      tagListener 请求tag监听事件，（给所有的请求添加tag，如果发起请求的activity或者fragment销毁了，就结束当前的请求）
     * @param isShowDialog     是否显示进度条
     * @param <T>
     */
    public <T> void sendPostRequest(Context context, String url, final Map<String, String> paramMap, Class<T> objectClass,
                                    AbstractActionCallbackListener<T> callbackListener, RequestTagListener tagListener, boolean isShowDialog) {
        handleFlushRequest(context, url, Request.Method.POST, paramMap, objectClass, callbackListener, tagListener, isShowDialog);
    }

    /**
     * send post 请求。请求时显示进度条
     *
     * @param context          上下文对象
     * @param url              请求地址
     * @param paramMap         参数map
     * @param objectClass      接口返回结果实例
     * @param callbackListener 接口返回回调
     * @param tagListener      tagListener 请求tag监听事件，（给所有的请求添加tag，如果发起请求的activity或者fragment销毁了，就结束当前的请求）
     * @param <T>
     */
    public <T> void sendPostRequestWithDialog(Context context, String url, final Map<String, String> paramMap, Class<T> objectClass,
                                              AbstractActionCallbackListener<T> callbackListener, RequestTagListener tagListener) {
        sendPostRequest(context, url, paramMap, objectClass, callbackListener, tagListener, true);
    }
}
