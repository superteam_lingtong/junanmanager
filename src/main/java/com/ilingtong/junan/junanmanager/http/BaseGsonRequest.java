package com.ilingtong.junan.junanmanager.http;

import android.app.Dialog;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ilingtong.junan.junanmanager.bean.BaseResultBean;
import com.ilingtong.junan.junanmanager.constant.Constants;
import com.ilingtong.junan.junanmanager.constant.ErrorEvent;
import com.ilingtong.junan.junanmanager.listener.AbstractActionCallbackListener;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wuqian on 2016/8/5.
 * mail: wuqian@ilingtong.com
 * Description:自定义的GsonRequest
 */
public class BaseGsonRequest<T> extends Request<T> {
    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";

    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    private AbstractActionCallbackListener mCallbackListener = null;
    private Gson mGson;
    private final Class<T> mClass;

    private String mUrl = null;

    /**
     * 返回数据是否需要缓存
     */
    private boolean mNeedCache = false;

    /**
     * 当前是不是json格式数据上传
     */
    private boolean isJsonRequest = false;
    /**
     * json请求信息
     */
    private String mRequestBody;
    /**
     * json请求信息
     */
    private BaseResultBean<T> mBaseResultBean;

    private Dialog mDialog;

    /**
     * 构造方法
     *
     * @param method           请求方法
     * @param url              请求地址
     * @param objectClass      解析结果类class (body)
     * @param callbackListener 结果回调listener
     * @param needCache        是否需要缓存
     */
    public BaseGsonRequest(int method, String url, Class<T> objectClass,
                           AbstractActionCallbackListener callbackListener, boolean needCache, Dialog dialog) {
        this(method, url, objectClass, callbackListener, needCache, null, dialog);
    }

    /**
     * 构造方法
     *
     * @param method           请求方法
     * @param url              请求地址
     * @param objectClass      解析结果类class (body)
     * @param callbackListener 结果回调listener
     * @param needCache        是否需要缓存
     * @param jsonString       json数据
     */
    public BaseGsonRequest(int method, String url, Class<T> objectClass,
                           AbstractActionCallbackListener callbackListener, boolean needCache, String jsonString, Dialog dialog) {
        super(method, url, null);
        this.mClass = objectClass;
        this.mCallbackListener = callbackListener;
        mGson = new Gson();
        mNeedCache = needCache;
        mUrl = url;
        mRequestBody = jsonString;
        isJsonRequest = !TextUtils.isEmpty(mRequestBody);
        this.mDialog = dialog;
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {//根据需要重载
        retryPolicy = new DefaultRetryPolicy(
                Constants.CONNECT_TIMEOUT_MS, // 2500*10
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // 1
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT); // 1f

        return super.setRetryPolicy(retryPolicy);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        Map<String, String> paramsMap = new HashMap<String, String>();
        return paramsMap;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> map = new HashMap<String, String>();
        return map;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        try {
            String json = new String(response.data, "utf-8");
            if (mClass == null) {
                mBaseResultBean = mGson.fromJson(json, BaseResultBean.class);
            } else {
                mBaseResultBean = mGson.fromJson(json, type(BaseResultBean.class, mClass));
            }
            if (Constants.YES.equals(mBaseResultBean.head.return_flag)) {
                return Response.success(mBaseResultBean.body, HttpHeaderParser.parseCacheHeaders(response));
            } else if (Constants.TOKEN_FAIL.equals(mBaseResultBean.head.return_flag)) {
                return Response.error(new LogicParseError(ErrorEvent.NETWORK_TOKEN_FAIL_EVENT, mBaseResultBean.head.return_message + "token失效"));
            } else {
                return Response.error(new LogicParseError(ErrorEvent.NETWORK_OTHER_ERROR_EVENT, mBaseResultBean.head.return_message+""));
            }
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mCallbackListener.onSuccess(response);
        dismissDialog();
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
        //LogicParseError代表逻辑错误
        if (mCallbackListener != null) {
            if (error instanceof LogicParseError) {
                mCallbackListener.onFailure(((LogicParseError) error).code, ((LogicParseError) error).message);
            } else {
                mCallbackListener.onFailure(ErrorEvent.NETWORK_VOLLEY_ERROR_EVENT, error.getMessage());
            }
        }
        dismissDialog();
    }

    @Override
    public void cancel() {
        super.cancel();
        dismissDialog();
    }

    private void dismissDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public String getBodyContentType() {
        if (isJsonRequest) {
            return PROTOCOL_CONTENT_TYPE;
        }
        return super.getBodyContentType();
    }

    @Override
    public byte[] getBody() {
        if (isJsonRequest) {
            try {
                return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
            } catch (UnsupportedEncodingException uee) {
                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                        mRequestBody, PROTOCOL_CHARSET);
                return null;
            }
        }
        try {
            return super.getBody();
        } catch (AuthFailureError e) {
            e.printStackTrace();
            return null;
        }
    }

    private ParameterizedType type(final Class raw, final Type... args) {
        return new ParameterizedType() {
            public Type getRawType() {
                return raw;
            }

            public Type[] getActualTypeArguments() {
                return args;
            }

            public Type getOwnerType() {
                return null;
            }
        };
    }

}
