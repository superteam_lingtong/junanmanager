package com.ilingtong.junan.junanmanager.bean;

import java.io.Serializable;

/**
 * Created by fengguowei on 2017/4/14.
 *  * used by 13003
 * @Description: 审核详情
 */

public class AuditDetailInfoBean implements Serializable {
    private String applicant_id;
    private String name;
    private String phone;
    private String veterans_card_number;
    private String apply_date;
    private String id_card;
    private String id_card_positive_pic;
    private String id_card_opposite_pic;
    private String is_veterans;
    private String discharged_pic;
    private String auditing_status;
    private String reject_reason;
    private String arm;
    private String applicant_type;

    public String getApplicant_id() {
        return applicant_id;
    }

    public void setApplicant_id(String applicant_id) {
        this.applicant_id = applicant_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVeterans_card_number() {
        return veterans_card_number;
    }

    public void setVeterans_card_number(String veterans_card_number) {
        this.veterans_card_number = veterans_card_number;
    }

    public String getApply_date() {
        return apply_date;
    }

    public void setApply_date(String apply_date) {
        this.apply_date = apply_date;
    }

    public String getId_card() {
        return id_card;
    }

    public void setId_card(String id_card) {
        this.id_card = id_card;
    }

    public String getId_card_positive_pic() {
        return id_card_positive_pic;
    }

    public void setId_card_positive_pic(String id_card_positive_pic) {
        this.id_card_positive_pic = id_card_positive_pic;
    }

    public String getId_card_opposite_pic() {
        return id_card_opposite_pic;
    }

    public void setId_card_opposite_pic(String id_card_opposite_pic) {
        this.id_card_opposite_pic = id_card_opposite_pic;
    }

    public String getIs_veterans() {
        return is_veterans;
    }

    public void setIs_veterans(String is_veterans) {
        this.is_veterans = is_veterans;
    }

    public String getDischarged_pic() {
        return discharged_pic;
    }

    public void setDischarged_pic(String discharged_pic) {
        this.discharged_pic = discharged_pic;
    }

    public String getAuditing_status() {
        return auditing_status;
    }

    public void setAuditing_status(String auditing_status) {
        this.auditing_status = auditing_status;
    }

    public String getReject_reason() {
        return reject_reason;
    }

    public void setReject_reason(String reject_reason) {
        this.reject_reason = reject_reason;
    }

    public String getArm() {
        return arm;
    }

    public void setArm(String arm) {
        this.arm = arm;
    }

    public String getApplicant_type() {
        return applicant_type;
    }

    public void setApplicant_type(String applicant_type) {
        this.applicant_type = applicant_type;
    }
}
