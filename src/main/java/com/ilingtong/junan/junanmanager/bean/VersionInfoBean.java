package com.ilingtong.junan.junanmanager.bean;

/**
 * Created by fengguowei on 2017/4/17.
 *  * used by 1001
 * @Description: 版本信息
 */

public class VersionInfoBean {
    public String has_update;    //(是否有更新
    public String force_update;    //是否强制更新
    public String recently_version_no;    //新版本号
    public String recently_version_tips;    //APP新版本描述
    public String recently_version_link;    //APP新版本下载链接URL
}
