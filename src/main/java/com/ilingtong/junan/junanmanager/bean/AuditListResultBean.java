package com.ilingtong.junan.junanmanager.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by fengguowei on 2017/4/14.
 *  * used by 13002
 * @Description: 审核列表
 */

public class AuditListResultBean implements Serializable {
    private String data_total_count;
    private List<AuditInfoBean> auditing_list;

    public String getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(String data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<AuditInfoBean> getAuditing_list() {
        return auditing_list;
    }

    public void setAuditing_list(List<AuditInfoBean> auditing_list) {
        this.auditing_list = auditing_list;
    }
}
