package com.ilingtong.junan.junanmanager.bean;

import java.io.Serializable;

/**
 * Created by fengguowei on 2017/4/14.
 * used by 13001
 * @Description: user information
 */

public class UserInfoBean implements Serializable {
    private String user_id;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
