package com.ilingtong.junan.junanmanager.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description:基础数据列表详情。
 * use by 2018接口
 */
public class BaseDataListInfoBean implements Serializable {
    public String base_data_type;
    public ArrayList<BaseDataInfoBean> data_list;
}
