package com.ilingtong.junan.junanmanager.bean;

/**
 * Created by wuqian on 2016/8/5.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class BaseHeaderBean {
    public String function_id;
    public String return_flag;
    public String return_message;
}
