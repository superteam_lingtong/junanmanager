package com.ilingtong.junan.junanmanager.bean;

import java.io.Serializable;

/**
 * Created by fengguowei on 2017/4/14.
 *  * used by 13003
 * @Description: 审核详情
 */

public class AuditDetailResultBean implements Serializable {
    private AuditDetailInfoBean auditing_info;

    public AuditDetailInfoBean getAuditing_info() {
        return auditing_info;
    }

    public void setAuditing_info(AuditDetailInfoBean auditing_info) {
        this.auditing_info = auditing_info;
    }
}
