package com.ilingtong.junan.junanmanager.bean;

import android.content.SharedPreferences;
import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/8/9.
 * mail: wuqian@ilingtong.com
 * Description:配置信息
 */
public class ConfigInfoBean implements Serializable {

    private String app_inner_no;
    private String userid;
    private String token;
    private String userType;   //用户区分
    private String loginActity;    //登录activity的地址。用于当token失效后跳转到主程序的登录页
    private SharedPreferences sp;

    /**
     * 构造方法 当configInfoBean被回收时调用该方法获取数据
     *
     * @param sp 存储configInfo的sharedpreference
     */
    public ConfigInfoBean(SharedPreferences sp) {
        this.sp = sp;
    }

    /**
     * 在主项目的application中必须调用该方法初始化版本号
     *
     * @param app_inner_no
     */
    public void initConfigInfo(String app_inner_no, String loginActity) {
        setApp_inner_no(app_inner_no);
        setLoginActity(loginActity);
    }

    /**
     * 登录成功调用该方法保存userid和token。设置islogin为true
     *
     * @param userid
     * @param token
     */
    public void loginSccussSettings(String userid, String token, String userType) {
        setUserid(userid);
        setToken(token);
        setUserType(userType);
    }

    public String getLoginActity() {
        if (TextUtils.isEmpty(loginActity)) {
            loginActity = sp.getString("loginActity", "");
        }
        return loginActity;
    }

    public void setLoginActity(String loginActity) {
        this.loginActity = loginActity;
        sp.edit().putString("loginActity", loginActity).commit();
    }
    public String getApp_inner_no() {
        if (TextUtils.isEmpty(app_inner_no)) {
            app_inner_no = sp.getString("app_inner_no", "");
        }
        return app_inner_no;
    }

    public void setApp_inner_no(String app_inner_no) {
        this.app_inner_no = app_inner_no;
        sp.edit().putString("app_inner_no", app_inner_no).commit();
    }
    public String getUserid() {
        if (TextUtils.isEmpty(userid)) {
            userid = sp.getString("userid", "");
        }
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
        sp.edit().putString("userid", userid).commit();
    }

    public String getToken() {
        if (TextUtils.isEmpty(token)) {
            token = sp.getString("token", "");
        }
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        sp.edit().putString("token", token).commit();
    }

    public String getUserType() {
        if (TextUtils.isEmpty(userType)){
            userType = sp.getString("userType","");
        }
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
        sp.edit().putString("userType",userType);
    }

}
