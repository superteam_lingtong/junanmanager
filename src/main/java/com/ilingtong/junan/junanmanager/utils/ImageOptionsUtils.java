package com.ilingtong.junan.junanmanager.utils;

import android.graphics.Bitmap;

import com.ilingtong.junan.junanmanager.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * Created by fengguowei on 2017/4/19.
 */

public class ImageOptionsUtils {
    //缩略图
    public static DisplayImageOptions getThumbnailOptions() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.ic_head_default)
                .showImageForEmptyUri(R.mipmap.ic_head_default)
                .showImageOnFail(R.mipmap.ic_head_default)
                .cacheInMemory(true).cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    // 军官证
    public static DisplayImageOptions getCertificatePhotoOptions() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.certificate_of_officers_photo_default)//下载过程中显示的图片
                .showImageForEmptyUri(R.mipmap.certificate_of_officers_photo_default)
                .showImageOnFail(R.mipmap.certificate_of_officers_photo_default)
                .cacheInMemory(true).cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    // 身份证
    public static DisplayImageOptions getIDPhotoOptions() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.id_card_photo_default)//下载过程中显示的图片
                .showImageForEmptyUri(R.mipmap.id_card_photo_default)
                .showImageOnFail(R.mipmap.id_card_photo_default)
                .cacheInMemory(true).cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }
}
