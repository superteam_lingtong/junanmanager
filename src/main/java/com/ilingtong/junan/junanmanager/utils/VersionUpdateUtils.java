package com.ilingtong.junan.junanmanager.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.ilingtong.junan.junanmanager.AppActionManager;
import com.ilingtong.junan.junanmanager.bean.VersionInfoBean;
import com.ilingtong.junan.junanmanager.listener.AbstractActionCallbackListener;
import com.ilingtong.junan.junanmanager.listener.RequestTagListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by fengguowei on 2016/11/2.
 */
public class VersionUpdateUtils {
    //版本更新回调
    private static AbstractActionCallbackListener<VersionInfoBean> updateCallbackListener;
    public static final int LOADING = 1;
    public static final int COMPLETED = 2;
    public static final int CANCELED = -1;
    public static final String FILE_NAME = "JunAnManager.apk";
    private int fileSize;
    private Activity mActivity;
    private ICheckVersion iCheckVersion;
    private RequestTagListener mTagListener;
    public VersionUpdateUtils(Activity activity, RequestTagListener tagListener, ICheckVersion iCheckVersion) {
        this.mActivity = activity;
        this.iCheckVersion = iCheckVersion;
        this.mTagListener = tagListener;
    }

    //检测版本
    public void checkVersion() {
        updateCallbackListener = new AbstractActionCallbackListener<VersionInfoBean>(mActivity) {
            @Override
            public void onSuccess(VersionInfoBean data) {
                iCheckVersion.getVersionSuccess(data);
            }
            @Override
            public void onFailure(String errorEvent, String message) {
                super.onFailure(errorEvent, message);
            }
        };
            AppActionManager.getVersionInfo(mActivity, getVersionName(), false, updateCallbackListener, mTagListener);
    }


    /**
     * 获取版本名
     *
     * @return
     */
    public String getVersionName() {
        String versionName = "1.0.0";
        try {
            versionName = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }


    //文件下载
    public void loadFile(VersionInfoBean updateVersionBodyBean) {
        if ("".equals(updateVersionBodyBean.recently_version_link)) {
            return;
        }
        URL url = null;
        HttpURLConnection httpConnection = null;
        InputStream inputStream = null;
        try {
            url = new URL(updateVersionBodyBean.recently_version_link);
            httpConnection = (HttpURLConnection) url.openConnection();
            fileSize = httpConnection.getContentLength();
            inputStream = httpConnection.getInputStream();
            FileOutputStream fileOutputStream = null;
            if (inputStream != null) {
                File file = new File(Environment.getExternalStorageDirectory(),
                        FILE_NAME);
                fileOutputStream = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                int ch = -1;
                float count = 0;
                while ((ch = inputStream.read(buf)) != -1) {
                    fileOutputStream.write(buf, 0, ch);
                    count += ch;
                    iCheckVersion.downLoadFileChanged(LOADING, (int) (count * 100 / fileSize));
                }
            }
            fileOutputStream.flush();
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            iCheckVersion.downLoadFileChanged(COMPLETED, 0);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * @author fengguowei
     * @ClassName: ICheckVersion
     * @Description: 更新主线程UI控件
     * @date 2016/11/3 11:43
     */

    public interface ICheckVersion {
        /**
         * @param updateVersionBodyBean 版本检测结果
         * @author fengguowei
         * @date 2016/11/3 11:44
         * @Title: ${enclosing_method}
         * @Description: 更新主线程UI控件
         */
        void getVersionSuccess(VersionInfoBean updateVersionBodyBean);

        /**
         * @param flag 操作类型
         *             progress 已下载文件
         * @author fengguowei
         * @date 2016/11/7 17:29
         * @Title: ${enclosing_method}
         * @Description: 文件下载进度
         */
        void downLoadFileChanged(int flag, int progress);
    }
}
