package com.ilingtong.junan.junanmanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.ilingtong.junan.junanmanager.bean.AuditDetailResultBean;
import com.ilingtong.junan.junanmanager.bean.AuditListResultBean;
import com.ilingtong.junan.junanmanager.bean.RequestParamHeadBean;
import com.ilingtong.junan.junanmanager.bean.UserInfoBean;
import com.ilingtong.junan.junanmanager.bean.VersionInfoBean;
import com.ilingtong.junan.junanmanager.constant.Constants;
import com.ilingtong.junan.junanmanager.constant.ErrorEvent;
import com.ilingtong.junan.junanmanager.constant.HttpConstants;
import com.ilingtong.junan.junanmanager.http.RequestHelper;
import com.ilingtong.junan.junanmanager.listener.AbstractActionCallbackListener;
import com.ilingtong.junan.junanmanager.listener.RequestTagListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fengguowei on 2017/4/14.
 */

public class AppActionManager {
    /**
     * 格式化入口参数，添加head信息
     *
     * @param requestParam 未添加head前的原始参数map
     * @param isSystem     是否是系统接口。为true时表示不需要设置userid和token。
     * @return
     */
    public static Map<String, String> getParams_gson(Map requestParam, boolean isSystem) {
        Map<String, String> params_gson = new HashMap();
        Gson mGson = new Gson();
        RequestParamHeadBean head;
        if (isSystem) {
            head = new RequestParamHeadBean("", "");
        } else {
            head = new RequestParamHeadBean(JunAnApplication.instance.getmConfigInfoBean().getUserid(), JunAnApplication.instance.getmConfigInfoBean().getToken());
        }
        requestParam.put("head", mGson.toJson(head));
        params_gson.put("parameters_json", mGson.toJson(requestParam).toString());
        return params_gson;
    }

    /**
     * 为需要userid和token的接口格式化入口参数，添加head信息。
     *
     * @param requestParam 未添加head前的原始参数map
     * @return
     */
    public static Map<String, String> getParams_gson(Map requestParam) {
        return getParams_gson(requestParam, false);
    }

    /**
     * 13001 登录
     * @param name 账号 不为空
     * @param psd  密码  不为空
     * @author fengguowei
     * @date 2017/4/14 16:29
     * @Title: ${enclosing_method}
     * @Description: 君安管理登录
     */

    public static void login(Context context, String name, String psd, boolean isShowDialog, AbstractActionCallbackListener<UserInfoBean> abstractActionCallbackListener, RequestTagListener requestTagListenere) {
        if (TextUtils.isEmpty(name)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.empty_name));
            return;
        }
        if (TextUtils.isEmpty(psd)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.empty_password));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("password", psd);
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.JUNAN_LOGIN_URL, params, UserInfoBean.class, abstractActionCallbackListener, requestTagListenere, isShowDialog);
    }

    /**
     *13002 获取审核列表
     * @param sort_key 排序key 加载更多
     * @param forward      翻页方向 1：向下/初期显示  2：向上
     * @param fetch_count  返回数据条数 空：全部显示   非空：指定件数显示
     * @author fengguowei
     * @date 2017/4/14 16:36
     * @Title: ${enclosing_method}
     * @Description: 获取待审核数据列表
     */

    public static void getAuditDataList(Context context,String sort_key, String forward, String fetch_count, boolean isShowDialog, AbstractActionCallbackListener<AuditListResultBean> abstractActionCallbackListener, RequestTagListener requestTagListenere) {
        Map<String, String> params = new HashMap<>();
        params.put("sort_key", sort_key);
        params.put("forward", forward);
        params.put("fetch_count", fetch_count);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.GET_AUDIT_LIST_URL, params, AuditListResultBean.class, abstractActionCallbackListener, requestTagListenere, isShowDialog);
    }

    /**
     * 13003 审核详情
     * @param apply_date   申请日期
     * @param applicant_id 申请人Id
     * @author fengguowei
     * @date 2017/4/14 16:50
     * @Title: ${enclosing_method}
     * @Description: 获取审核详情
     */

    public static void getAuditDetail(Context context, String apply_date, String applicant_id, boolean isShowDialog, AbstractActionCallbackListener<AuditDetailResultBean> abstractActionCallbackListener, RequestTagListener requestTagListenere) {
        if (TextUtils.isEmpty(apply_date) || TextUtils.isEmpty(applicant_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.missing_parameters));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("apply_date", apply_date);
        params.put("applicant_id", applicant_id);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.GET_AUDIT_DETAIL_URL, params, AuditDetailResultBean.class, abstractActionCallbackListener, requestTagListenere, isShowDialog);
    }

    /**
     * 13004 审核同意/拒绝
     * @param apply_date   申请日期
     * @param applicant_id 申请人Id
     * @param status       审核状态 1-审核通过  2-审核拒否
     * @author fengguowei
     * @date 2017/4/14 16:56
     * @Title: ${enclosing_method}
     * @Description: 审核操作
     */

    public static void auditAction(Context context, String apply_date, String applicant_id, String status, String reject_reason, boolean isShowDialog, AbstractActionCallbackListener abstractActionCallbackListener, RequestTagListener requestTagListenere) {
        if (TextUtils.isEmpty(apply_date) || TextUtils.isEmpty(applicant_id) || TextUtils.isEmpty(status)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.missing_parameters));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("apply_date", apply_date);
        params.put("applicant_id", applicant_id);
        params.put("status", status);
        params.put("reject_reason", reject_reason);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.AUDIT_ACTION_URL, params, null, abstractActionCallbackListener, requestTagListenere, isShowDialog);
    }

    /**
     * 1001 版本更新
     * @param local_app_version 本地版本号
     * @return
     * @throws
     * @author fengguowei
     * @date 2017/4/17 17:28
     * @Title: ${enclosing_method}
     * @Description: 获取版本信息
     */

    public static void getVersionInfo(Context context, String local_app_version, boolean isShowDialog, AbstractActionCallbackListener<VersionInfoBean> abstractActionCallbackListener, RequestTagListener requestTagListenere) {
        Map<String, String> params = new HashMap<>();
        params.put("mobile_os_no", Constants.PHONE);
        params.put("local_app_version", local_app_version);
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.UPDATEVERSION_URL, params, VersionInfoBean.class, abstractActionCallbackListener, requestTagListenere, isShowDialog);
    }

}
